In this project we’re going to build an app that lets the user build a private list of places on the map that they intend to visit one day, add a description for that place, look up interesting places that are nearby, and save it all to the iOS storage for later.

MVVM, MapKit, FaceID, CoreLocation, LocalAuthentication


<img src="/uploads/a1e7be8c399ed3c20f6b13e40be17059/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-02-13_at_17.34.00.png" width="320" >

<img src="/uploads/be60f72245a8a1a726ccb62a6ae8a4d6/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-02-13_at_17.39.36.png" width="320" >

<img src="/uploads/92dfc5ed3f29087c7042ad7c45bda994/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-02-13_at_17.39.52.png" width="320" >
