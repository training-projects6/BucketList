//
//  EditView.swift
//  BucketList
//
//  Created by Artem Soloviev on 04.02.2023.
//

import SwiftUI

struct EditView: View {
    
    @Environment(\.dismiss) var dismiss
   
    @StateObject private var editViewModel: EditViewModel
    var onSave: (Location) -> Void

    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("New location", text: $editViewModel.name)
                    TextField("Description", text: $editViewModel.description)
                }
                Section("Nearby…") {
                    switch editViewModel.loadingState {
                    case .loaded:
                        ForEach(editViewModel.pages, id: \.pageid) { page in
                            Text(page.title)
                                .font(.headline)
                            + Text(": ")
                            + Text(page.description)
                                .italic()
                        }
                    case .loading:
                        Text("Loading…")
                    case .failed:
                        Text("Please try again later.")
                    }
                }
            }
            .navigationTitle("Place details")
            .toolbar {
                Button("Save") {
                    let newLocation =  editViewModel.newLocation()
                    onSave(newLocation)
                    dismiss()
                }
                .task {
                    await editViewModel.fetchNearbyPlaces()
                }
            }
        }
    }

    init(location: Location, onSave: @escaping (Location) -> Void) {
         _editViewModel = StateObject(wrappedValue: EditViewModel(location: location))
         self.onSave = onSave
     }

}

//struct EditView_Previews: PreviewProvider {
//    static var previews: some View {
//        EditView(location: Location.example) { newLocation in }
//    }
//}
