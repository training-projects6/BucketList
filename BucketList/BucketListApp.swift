//
//  BucketListApp.swift
//  BucketList
//
//  Created by Artem Soloviev on 02.02.2023.
//

import SwiftUI

@main
struct BucketListApp: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
